       IDENTIFICATION DIVISION.
       PROGRAM-ID. DATA3.
       AUTHOR. RENU.
       DATA DIVISION.
       WORKING-STORAGE SECTION. 
       01  NUM1  PIC   99.
       01  NUM2  PIC   99.
       01  NUM3  PIC   99.
       01  NUM4  PIC   99.
       01  PROBLEM-STRING PIC X(60).

       PROCEDURE DIVISION.
           PERFORM PROBLEM1
           PERFORM PROBLEM2
           PERFORM  PROBLEM3
           PERFORM  PROBLEM4
           PERFORM  PROBLEM5 
           PERFORM  PROBLEM6
           PERFORM  PROBLEM7
           PERFORM  PROBLEM8
           PERFORM  PROBLEM9
           PERFORM  PROBLEM10
           PERFORM  PROBLEM11
           PERFORM  PROBLEM12
           PERFORM  PROBLEM13

           GOBACK
           .
       HEADER.
           DISPLAY "***************************************************"
           DISPLAY PROBLEM-STRING 
           DISPLAY "        NUM1  NUM2  NUM3  NUM4"
           EXIT 
           .
       DISPLAYBEFORE.
           DISPLAY "BEFORE    " NUM1 "    " NUM2 "    " NUM3 "    " NUM4
           .
       DISPLAYAFTER.
           DISPLAY "AFTER     " NUM1 "    " NUM2 "    " NUM3 "    " NUM4
           .

       PROBLEM1.
           MOVE "PROBLEM1 : ADD NUM1 TO NUM2" TO PROBLEM-STRING 
           MOVE  25 TO NUM1 
           MOVE  30 TO NUM2
           MOVE  ZERO TO  NUM3 
           MOVE  ZERO TO  NUM4 
           PERFORM HEADER
           PERFORM DISPLAYBEFORE 
           ADD NUM1 TO NUM2
           PERFORM DISPLAYAFTER
           EXIT
       .
       PROBLEM2.
           MOVE "PROBLEM2 : ADD NUM1, NUM2 TO NUM3, NUM4" TO 
           PROBLEM-STRING 
           MOVE  13 TO    NUM1 
           MOVE  04 TO    NUM2
           MOVE  05 TO    NUM3 
           MOVE  12 TO    NUM4 
           PERFORM HEADER
           PERFORM DISPLAYBEFORE 
           ADD NUM1, NUM2 TO NUM3, NUM4
           PERFORM DISPLAYAFTER
           EXIT
           .
       PROBLEM3.
           MOVE "PROBLEM3 : ADD NUM1, NUM2, NUM3 GIVING NUM4" TO 
           PROBLEM-STRING 

           MOVE  04 TO    NUM1 
           MOVE  03 TO    NUM2
           MOVE  02 TO    NUM3 
           MOVE  01 TO    NUM4 
           PERFORM HEADER
           PERFORM DISPLAYBEFORE 
           ADD NUM1, NUM2, NUM3 GIVING NUM4 
           PERFORM DISPLAYAFTER 
           EXIT 
           .
      
       PROBLEM4.
           MOVE "PROBLEM4 : SUBTRACT NUM1 FROM  NUM2 GIVING NUM3 " TO 
           PROBLEM-STRING 

           MOVE  04 TO    NUM1 
           MOVE  10 TO    NUM2
           MOVE  55 TO    NUM3 
           MOVE  ZERO  TO    NUM4 
           PERFORM HEADER
           PERFORM DISPLAYBEFORE 
           SUBTRACT NUM1 FROM  NUM2 GIVING NUM3 
           PERFORM DISPLAYAFTER 
           EXIT 
           .
       PROBLEM5.
           MOVE "PROBLEM5 : SUBTRACT NUM1, NUM2 FROM NUM3 " TO 
           PROBLEM-STRING 

           MOVE  05 TO    NUM1 
           MOVE  10 TO    NUM2
           MOVE  55 TO    NUM3 
           MOVE  ZERO  TO    NUM4 
           PERFORM HEADER
           PERFORM DISPLAYBEFORE 
           SUBTRACT NUM1, NUM2 FROM NUM3 
           PERFORM DISPLAYAFTER 
           EXIT 
           .
       PROBLEM6.
           MOVE "PROBLEM6 : SUBTRACT NUM1, NUM2 FROM NUM3 GIVING NUM4" 
           TO PROBLEM-STRING 

           MOVE  05 TO    NUM1 
           MOVE  10 TO    NUM2
           MOVE  55 TO    NUM3 
           MOVE  20 TO    NUM4 
           PERFORM HEADER
           PERFORM DISPLAYBEFORE 
           SUBTRACT NUM1, NUM2 FROM NUM3 GIVING NUM4
           PERFORM DISPLAYAFTER 
           EXIT 
           .
       PROBLEM7.
           MOVE "PROBLEM7 : MULTIPLY  NUM1 BY NUM2" 
           TO PROBLEM-STRING 

           MOVE  10       TO    NUM1 
           MOVE  05       TO    NUM2
           MOVE  ZERO     TO    NUM3 
           MOVE  ZERO     TO    NUM4 
           PERFORM HEADER
           PERFORM DISPLAYBEFORE 
           MULTIPLY  NUM1 BY NUM2
           PERFORM DISPLAYAFTER 
           EXIT 
           .
       PROBLEM8.
           MOVE "PROBLEM8 : MULTIPLY  NUM1 BY NUM2 GIVING NUM3" 
           TO PROBLEM-STRING 

           MOVE  10       TO    NUM1 
           MOVE  05       TO    NUM2
           MOVE  33       TO    NUM3 
           MOVE  ZERO     TO    NUM4 
           PERFORM HEADER
           PERFORM DISPLAYBEFORE 
           MULTIPLY  NUM1 BY NUM2 GIVING NUM3
           PERFORM DISPLAYAFTER 
           EXIT 
           .
       PROBLEM9.
           MOVE "PROBLEM9 : DIVIDE  NUM1 INTO NUM2" 
           TO PROBLEM-STRING 

           MOVE  05       TO    NUM1 
           MOVE  64       TO    NUM2
           MOVE  ZERO     TO    NUM3 
           MOVE  ZERO     TO    NUM4 
           PERFORM HEADER
           PERFORM DISPLAYBEFORE 
           DIVIDE  NUM1 INTO NUM2
           PERFORM DISPLAYAFTER 
           EXIT 
           .
       PROBLEM10.
           MOVE "PROBLEM10 : DIVIDE NUM2 BY NUM1 GIVING NUM3 REMAINDER NU
      -    "M4" TO PROBLEM-STRING 

           MOVE  05       TO    NUM1 
           MOVE  64       TO    NUM2
           MOVE  ZERO     TO    NUM3 
           MOVE  ZERO     TO    NUM4 
           PERFORM HEADER
           PERFORM DISPLAYBEFORE 
           DIVIDE  NUM2 BY NUM1 GIVING NUM3 REMAINDER NUM4 
           PERFORM DISPLAYAFTER 
           EXIT 
           .
       PROBLEM11.
           MOVE "PROBLEM11 : COMPUTE NUM1 = 5 + 10 * 30 / 2" 
           TO PROBLEM-STRING 

           MOVE  25       TO    NUM1 
           MOVE  ZERO     TO    NUM2
           MOVE  ZERO     TO    NUM3 
           MOVE  ZERO     TO    NUM4 
           PERFORM HEADER
           PERFORM DISPLAYBEFORE 
           COMPUTE NUM1 = 5 + 10 * 30 / 2
           PERFORM DISPLAYAFTER 
           EXIT 
           .

       PROBLEM12.
           MOVE "PROBLEM12 : ON SIZE ERROR" TO PROBLEM-STRING 

           PERFORM HEADER
           PERFORM DISPLAYBEFORE 
           COMPUTE NUM1 = 99 + 1
              ON SIZE ERROR DISPLAY "----------ON-SIZE-ERROR-----------"
           END-COMPUTE 
           PERFORM DISPLAYAFTER 
           EXIT 
           .

       PROBLEM13.
           DISPLAY "***************************************************"
           DISPLAY "PLEASE INPUT FIRST NUM: " WITH NO ADVANCING 
           ACCEPT NUM1 
           DISPLAY "PLEASE INPUT SECOND NUM: " WITH NO ADVANCING 
           ACCEPT NUM2 
           COMPUTE NUM3 = NUM2 + NUM1 ON SIZE ERROR
              DISPLAY "----------ON-SIZE-ERROR-----------"
           END-COMPUTE 
           DISPLAY "NUM3 : " NUM3 
           EXIT 
           .
